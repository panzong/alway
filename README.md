


## 搭建步骤

1. 连接**SSH**，执行以下命令

### vmess

```shell
wget -N https://gitlab.com/panzong/alway/-/raw/main/niub.sh && chmod +x ./niub.sh
```

### vless

```shell
wget -N https://gitlab.com/panzong/alway/-/raw/main/niubvl.sh && chmod +x ./niubvl.sh
```

### trojan

```shell
wget -N https://gitlab.com/panzong/alway/-/raw/main/niubtr.sh && chmod +x ./niubtr.sh
```

2. 修改网站空间配置

## 客户端配置

vmess / vless

```
地址：appname.alwaysdata.net
端口：443
默认UUID：cb2ef9cb-fc3a-4f42-a206-0a59919a38d6
vmess额外id：0
加密：none
传输协议：ws
伪装类型：none
伪装域名：appname.fly.dev
路径：/
底层传输安全：tls
跳过证书验证：false
```

Trojan-go

```bash
{
    "run_type": "client",
    "local_addr": "127.0.0.1",
    "local_port": 1080,
    "remote_addr": "appname.alwaysdata.net",
    "remote_port": 443,
    "password": [
        "cb2ef9cb-fc3a-4f42-a206-0a59919a38d6"
    ],
    "websocket": {
        "enabled": true,
        "path": "/",
        "host": "appname.alwaysdata.net"
    }
}
```

## 感谢列表

原作者项目：https://github.com/wgp-2020/AX
